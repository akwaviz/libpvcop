//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <pvcop/utils/filesystem.h>

#include <algorithm>
#include <iterator>
#include <fstream>
#include <tuple>

#include <cerrno>
#include <climits>

#include <sys/stat.h>

/*****************************************************************************
 * pvcop::filesystem::check_access
 *****************************************************************************/

bool pvcop::filesystem::check_access(const std::string& path,
                                     pvcop::filesystem::access_mode m,
                                     pvcop::filesystem::entry_type t)
{
	struct stat dir_stat;

	const int amode = (m == access_mode::READ) ? (S_IRUSR | S_IRGRP) : (S_IWUSR | S_IWGRP);

	const int atype = (t == entry_type::FILE) ? S_IFREG : S_IFDIR;

	if (stat(path.c_str(), &dir_stat) == 0) {
		if ((dir_stat.st_mode & atype) == 0) {
			return false;
		} else if ((dir_stat.st_mode & amode) == 0) {
			return false;
		}
	} else {
		return false;
	}

	return true;
}

/*****************************************************************************
 * pvcop::filesystem::make_path
 *****************************************************************************/

int pvcop::filesystem::make_path(const std::string& path)
{
	if (path.size() >= PATH_MAX) {
		return ENAMETOOLONG;
	}

	std::string tmp_path;

	tmp_path.reserve(PATH_MAX);

	size_t base = 0;
	size_t next;

	struct stat dir_stat;

	int ret;

	while (true) {

		// search for the next directory name
		next = path.find_first_of('/', base);

		if (next == base) {
			/* As base can never be equal to std::string ::npos,
			 * this is the case where we have a separator instead
			 * of a directory name; it happens when we encounter
			 * the leading '/' of an absolute path or when the
			 * path contains "//"
			 */
			base = next + 1;
			continue;
		}

		// now, we are sure to have a non-empty directory name
		size_t dir_len = next - base;

		if (next == std::string::npos) {
			/* As std::string::npos is a "-1", the length
			 * computation arithmetic changes...
			 */
			dir_len = path.size() - base;
		}

		if (base == 0) {
			/* case of a heading relative path; i.e.
			 * path == "../a/path/"
			 */
			tmp_path.append(path, base, dir_len);
		} else {
			// all other cases
			tmp_path.append("/");
			tmp_path.append(path, base, dir_len);
		}

		errno = 0;
		ret = stat(tmp_path.c_str(), &dir_stat);
		if (ret == 0) {
			if (!S_ISDIR(dir_stat.st_mode)) {
				// the path does not refer to a directory
				ret = ENOTDIR;
				break;
			}
		} else if (errno == ENOENT) {
			/* the path does not named anything; we can also
			 * create the directory
			 */
			ret = mkdir(tmp_path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP);
			if (ret < 0) {
				// mkdir fails, no way to bypass that case
				break;
			}
		} else {
			// stat fails for any other reason
			break;
		}

		if (next == std::string::npos) {
			// no more '/' to search for
			break;
		}

		base = next + 1;
	}

	if ((ret == 0) && check_access(tmp_path, access_mode::WRITE, entry_type::DIR) == false) {
		// last check for a writable directory
		ret = EPERM;
	}

	return ret;
}

/*****************************************************************************
 * pvcop::filesystem::sanitize_directory_path
 *****************************************************************************/

void pvcop::filesystem::sanitize_directory_path(std::string& path)
{
	// if there is no trailing '/', we add one
	if (path.back() != '/') {
		path.append("/");
	}
}

/*****************************************************************************
 * pvcop::filesystem::compare_files
 *****************************************************************************/

bool pvcop::filesystem::compare_files(const char* filename1, const char* filename2)
{
	std::ifstream file1(filename1);
	std::ifstream file2(filename2);

	file1.seekg(0, std::ios_base::end);
	file2.seekg(0, std::ios_base::end);

	if (file1.tellg() != file2.tellg()) {
		return false;
	}

	std::istreambuf_iterator<char> begin1(file1);
	std::istreambuf_iterator<char> begin2(file2);

	std::istreambuf_iterator<char> end;

	std::tie(begin1, begin2) = std::mismatch(begin1, end, begin2);
	return begin1 == end and begin2 == end;
}
