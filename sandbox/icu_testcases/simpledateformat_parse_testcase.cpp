//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <assert.h>
#include <iostream>
#include <limits>
#include <memory>
#include <ctime>

#include <unicode/smpdtfmt.h>

static constexpr UDate date = 90232436000UL;

void icu_testcase()
{
	std::cout << (uint64_t)date << " -> ";

	UErrorCode err = U_ZERO_ERROR;
	UnicodeString pattern("yyyy.M.d 'at' a H:m:ss,SSS v");
	// UnicodeString pattern("yyyy.MM.dd 'at' a HH:mm:ss,SSS v"); // no bug (am-pm marker is
	// specified
	// before 24h
	// formatted hour)
	// UnicodeString pattern("yyyy.MM.dd 'at' h:mm:ss,SSS a v");  // no bug (am-pm marker is
	// specified
	// after  12h
	// formatted hour)
	const SimpleDateFormat dfmt(pattern, err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creation SimpleDateFormat instance" << std::endl;
	}

	std::unique_ptr<Calendar> calendar(Calendar::createInstance(err));
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creating Calendar instance" << std::endl;
	}

	calendar->setTime((UDate)date, err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when setting Calendar time" << std::endl;
	}

	// Format UDate to UnicodeString
	UnicodeString res;
	FieldPosition fp(0);
	dfmt.format(*calendar, res, fp);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when formatting Calendar" << std::endl;
	}
	std::string s;
	res.toUTF8String(s);
	std::cout << s << " -> ";

	// Parse UnicodeString to UDate
	calendar->setTime(0, err);
	ParsePosition p(0);
	dfmt.parse(res, *calendar, p);
	UDate date2 = calendar->getTime(err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when getting time from Calendar" << std::endl;
	}

	// Format UDate to UnicodeString again
	calendar->setTime(0, err); // force date to reset
	calendar->setTime(date2, err);
	std::cout << (uint64_t)date2 << " -> ";
	res = "";
	s.clear();
	dfmt.format(*calendar, res, fp);
	res.toUTF8String(s);
	std::cout << s << std::endl;

	assert(date == date2); // dates should be egal
}

void libc_testcase()
{
	setenv("TZ", "GMT", 1);

	static constexpr char pattern[] = "%Y.%m.%d at %H:%M:%S %p %Z";
	static constexpr size_t str_len = 1024;
	char str[str_len];
	tm tm_struct;
	time_t t = (uint64_t)date / 1000;

	std::cout << t << " -> ";

	// Format time_t to char*
	std::strftime(str, str_len, pattern, gmtime_r(&t, &tm_struct));
	std::cout << str << " -> ";

	// Parse char* to time_t
	tm_struct = tm(); // force date to reset
	strptime(str, pattern, &tm_struct);
	time_t date2 = mktime(&tm_struct);
	std::cout << date2 << " -> ";

	// Format time_t to char* again
	std::strftime(str, str_len, pattern, gmtime_r(&date2, &tm_struct));
	std::cout << str << std::endl;

	assert(t == date2); // dates are egal
}

int main()
{
	std::cout << "libc reference testcase:" << std::endl;
	libc_testcase();

	std::cout << std::endl << "ICU testcase:" << std::endl;
	icu_testcase();
}
