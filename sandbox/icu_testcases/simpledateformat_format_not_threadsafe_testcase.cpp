//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <algorithm>
#include <assert.h>
#include <iostream>
#include <vector>
#include <memory>

#include <unicode/gregocal.h>
#include <unicode/smpdtfmt.h>

//#define SIMPLE_DATE_FORMAT_THREAD_LOCAL // No bug when using a thread_local SimpleDateFormat
// instance

static uint64_t parse(const std::string& value, const SimpleDateFormat& dfmt)
{
	UErrorCode err = U_ZERO_ERROR;

	thread_local static std::unique_ptr<Calendar> calendar(Calendar::createInstance(err));
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creating Calendar instance" << std::endl;
		return 0;
	}

	ParsePosition p(0);
	dfmt.parse(UnicodeString(value.c_str()), *calendar, p);

	uint64_t ret = (uint64_t)calendar->getTime(err);

	if (not U_SUCCESS(err)) {
		std::cerr << "Error when getting time from Calendar" << std::endl;
		return 0;
	}

	return ret;
}

static std::string format(uint64_t value, const SimpleDateFormat& dfmt)
{
	UErrorCode err = U_ZERO_ERROR;

	thread_local static std::unique_ptr<Calendar> calendar(Calendar::createInstance(err));
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creating Calendar instance" << std::endl;
		return 0;
	}

	calendar->setTime((UDate)value, err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when setting Calendar time" << std::endl;
		return 0;
	}

	UnicodeString res;
	FieldPosition fp(0);

#ifndef SIMPLE_DATE_FORMAT_THREAD_LOCAL
	dfmt.format(*calendar, res, fp);
#else
	thread_local static const SimpleDateFormat dfmt_thread_local(
	    UnicodeString("yyyy.M.d 'at' HH:mm:ss,SSS v"), err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creation SimpleDateFormat instance" << std::endl;
		return 0;
	}
	dfmt_thread_local.format(*calendar, res, fp);
#endif

	if (not U_SUCCESS(err)) {
		std::cerr << "Error when formatting Calendar to string" << std::endl;
		return 0;
	}

	std::string s;
	res.toUTF8String(s);

	return s;
}

int main()
{
	static constexpr size_t count = 10000;
	static constexpr uint64_t timestamp = 1445526487123UL;

	UErrorCode err = U_ZERO_ERROR;
	const SimpleDateFormat dfmt(UnicodeString("yyyy.M.d 'at' HH:mm:ss,SSS v"), err);
	if (not U_SUCCESS(err)) {
		std::cerr << "Error when creation SimpleDateFormat instance" << std::endl;
	}

	std::vector<uint64_t> timestamp_array1;
	std::vector<uint64_t> timestamp_array2;
	std::vector<std::string> string_array;

	timestamp_array1.resize(count);
	timestamp_array2.resize(count);
	string_array.resize(count);

	std::fill(timestamp_array1.begin(), timestamp_array1.end(), timestamp);
	std::fill(timestamp_array2.begin(), timestamp_array2.end(), 0);

// Format to strings in parallel
#pragma omp parallel for
	for (size_t i = 0; i < count; i++) {
		string_array[i] = format(timestamp_array1[i], dfmt);
	}

	// Parse strings back
	for (size_t i = 0; i < count; i++) {
		timestamp_array2[i] = parse(string_array[i], dfmt);
	}

	const auto& mismatching_timestamps =
	    std::mismatch(timestamp_array2.begin(), timestamp_array2.end(), timestamp_array1.begin());

	if (mismatching_timestamps.first != timestamp_array2.end()) {
		std::cout << *mismatching_timestamps.first << " -> ";
		std::cout << *mismatching_timestamps.second << std::endl;
	}

	assert(mismatching_timestamps.first == timestamp_array2.end());
}
