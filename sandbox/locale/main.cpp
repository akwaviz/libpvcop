//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <cstdio>
#include <iostream>
#include <clocale>
#include <locale>

void do_work()
{
	printf("long printf  : %'ld\n", 123456789L);
	printf("double printf: %'f\n", 123456.789);
	std::cout << "long cout    : " << 123456789L << std::endl;
	;
	std::cout << "double count : " << 123456.789 << std::endl;
	;
}

int main()
{
	printf("default locale is: %s\n", setlocale(LC_ALL, NULL));
	do_work();

	setlocale(LC_ALL, "");
	printf("env locale is: %s\n", setlocale(LC_ALL, NULL));
	do_work();

	std::locale loc("fr_FR.utf8");
	std::locale::global(loc);
	printf("fr_FR.utf8 locale is: %s\n", setlocale(LC_ALL, NULL));

	do_work();

	return 0;
}
