//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>
#include <stddef.h>
#include <sys/time.h>
#include <limits>

#include "test.h"

static inline double now()
{
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return (double)tv.tv_sec + (tv.tv_usec / 1000000.);
}

int main()
{
	Test test;

	srand(time(NULL));

	{
		double start = now();
		size_t res = test.func_pointer([](size_t sum, size_t val) { return sum + val; });
		double end = now();

		std::cout << "func_pointer:" << (end - start) << " (" << res << ")" << std::endl;
	}

	{
		double start = now();
		size_t res = test.inlined_func_pointer([](size_t sum, size_t val) { return sum + val; });
		double end = now();

		std::cout << "inlined_func_pointer:" << (end - start) << " (" << res << ")" << std::endl;
	}

	{
		double start = now();
		size_t res = test.func_template([](size_t sum, size_t val) { return sum + val; });
		double end = now();

		std::cout << "func_template:" << (end - start) << " (" << res << ")" << std::endl;
	}

	{
		double start = now();
		size_t res = test.std_func([](size_t sum, size_t val) { return sum + val; });
		double end = now();

		std::cout << "std_func:" << (end - start) << " (" << res << ")" << std::endl;
	}

	{
		double start = now();
		size_t res = test.inlined_std_func([](size_t sum, size_t val) { return sum + val; });
		double end = now();

		std::cout << "inlined_std_func:" << (end - start) << " (" << res << ")" << std::endl;
	}
}
