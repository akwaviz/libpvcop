/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef TEST_H_
#define TEST_H_

#include <functional>
#include <stdlib.h> /* srand, rand */

constexpr size_t MAX_ITER = 4L * 1024 * 1024 * 1024;

class Test
{
  public:
	Test();

	size_t __attribute__((flatten)) func_pointer(size_t (*func)(size_t, size_t)) const;

	virtual inline size_t inlined_func_pointer(size_t (*func)(size_t, size_t)) const
	{
		size_t res = 0;

		for (size_t i = 0; i < MAX_ITER; i++) {
			res = func(res, _array[i]);
		}

		return res;
	}

	template <typename F>
	inline size_t func_template(const F& func) const
	{
		size_t res = 0;

		for (size_t i = 0; i < MAX_ITER; i++) {
			res = func(res, _array[i]);
		}

		return res;
	}

	size_t std_func(std::function<size_t(size_t, size_t)> func) const;

	inline size_t inlined_std_func(std::function<size_t(size_t, size_t)> func) const
	{
		size_t res = 0;

		for (size_t i = 0; i < MAX_ITER; i++) {
			res = func(res, _array[i]);
		}

		return res;
	}

  private:
	size_t* _array;
};

#endif /* TEST_H_ */
