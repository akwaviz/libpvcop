//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include "test.h"

Test::Test()
{
	_array = new size_t[MAX_ITER];
	for (size_t i = 0; i < MAX_ITER; i++) {
		_array[i] = rand() % 255;
	}
}

size_t Test::func_pointer(size_t (*func)(size_t, size_t)) const
{
	size_t res = 0;

	for (size_t i = 0; i < MAX_ITER; i++) {
		res = func(res, _array[i]);
	}

	return res;
}

size_t Test::std_func(std::function<size_t(size_t, size_t)> func) const
{
	size_t res = 0;

	for (size_t i = 0; i < MAX_ITER; i++) {
		res = func(res, _array[i]);
	}

	return res;
}
