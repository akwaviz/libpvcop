//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>
#include <typeinfo>

struct bit {
	int data : 1;
};

template <typename T>
class array_storage_traits
{
  public:
	using storage_type = T;
};

template <>
class array_storage_traits<bool>
{
  public:
	using storage_type = bit;
};

template <typename T>
class array
{
  public:
	using type = T;

	void print_storage_type()
	{
		std::cout << typeid(typename array_storage_traits<T>::storage_type).name() << std::endl;
	}
};

int main()
{
	array<int> a_int;
	a_int.print_storage_type();

	array<double> a_double;
	a_double.print_storage_type();

	array<bool> a_bool;
	a_bool.print_storage_type();
}
