//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <boost/date_time/time_facet.hpp>
#include <boost/date_time/time_clock.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <iostream>
#include <sstream>
#include <typeinfo>

union un {
	un(boost::posix_time::ptime p) : ptime(p){};

	boost::posix_time::ptime ptime;
	uint64_t timestamp;
};

int main()
{
	static constexpr char pattern[] = "%Y.%m.%d %H.%M.%S.%F";
	std::locale in_loc(
	    std::locale(std::locale::classic(), new boost::posix_time::time_input_facet(pattern)));
	std::locale out_loc(
	    std::locale(std::locale::classic(), new boost::posix_time::time_facet(pattern)));

	// boost::posix_time::ptime pt = boost::posix_time::microsec_clock::universal_time();
	uint64_t date1 = 1445526487772204UL;
	boost::posix_time::ptime pt =
	    boost::posix_time::from_time_t(0) + boost::posix_time::microseconds(date1);

	std::cout << "date1=" << date1 << std::endl;

	std::stringstream date_stream;
	date_stream.imbue(in_loc);
	date_stream << pt;
	std::cout << "str1=" << date_stream.str() << std::endl;

	std::istringstream ds(date_stream.str());
	boost::posix_time::ptime t;

	ds.imbue(out_loc);
	ds >> t;

	boost::posix_time::time_duration dur2 = t - boost::posix_time::from_time_t(0);
	uint64_t date2 = dur2.total_microseconds();

	std::cout << "date2=" << date2 << std::endl;

	date_stream.str("");
	date_stream << t;
	std::cout << "str2=" << date_stream.str() << std::endl;

	/// Reinterpret cast:

	boost::gregorian::date d1{10000, 1, 1};
	boost::posix_time::ptime pt2(d1);

	std::cout << "pt2=" << pt2 << std::endl;

	un u(pt2);
	std::cout << u.timestamp << std::endl;

	uint64_t value2 = *reinterpret_cast<uint64_t*>(&pt2);
	std::cout << value2 << std::endl;

	value2 = 464269579539000000UL;

	std::cout << "sizeof(boost::posix_time::ptime)=" << sizeof(boost::posix_time::ptime)
	          << std::endl;
	std::cout << "std::numeric_limits<uint64_t>::max()=" << std::numeric_limits<uint64_t>::max()
	          << std::endl;

	const boost::posix_time::ptime t2 = *reinterpret_cast<const boost::posix_time::ptime*>(&value2);
	std::cout << t2 << std::endl;

	{
		std::locale in_loc(
		    std::locale(std::locale::classic(),
		                new boost::posix_time::time_input_facet("%Y.%m.%d %H.%M.%S%F")));
		std::locale out_loc(std::locale(std::locale::classic(),
		                                new boost::posix_time::time_facet("%Y.%m.%d %H.%M.%S%F")));

		uint64_t date1 = 1445526487772204UL;
		boost::posix_time::ptime pt =
		    boost::posix_time::from_time_t(0) + boost::posix_time::microseconds(date1);

		std::stringstream date_stream;
		date_stream.imbue(out_loc);
		date_stream << pt;
		std::cout << "str1=" << date_stream.str() << std::endl;

		std::istringstream ds(date_stream.str());
		boost::posix_time::ptime t;
		ds.imbue(in_loc);
		ds >> t;

		date_stream.str("");
		date_stream << t;
		std::cout << "str2=" << date_stream.str() << std::endl;
	}
}
