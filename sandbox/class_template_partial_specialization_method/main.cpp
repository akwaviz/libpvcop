//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>

using namespace std;

class B
{
  public:
	B() {}
	virtual ~B() {}

  public:
	virtual void f() = 0;
};

template <class T>
class D : public B
{
  public:
	D() {}
	virtual ~D() {}

  public:
	virtual void f() { cout << "T type" << endl; }
};

// Class partial specialization
template <>
class D<bool> : public B
{
  public:
	D(){};
	virtual ~D() {}

  public:
	virtual void f() { cout << "bool type" << endl; }
};

// Method partial specialization
template <>
void D<double>::f()
{
	cout << "double type" << endl;
}
template <>
D<double>::D()
{
	cout << "double cstor" << endl;
}

int main()
{
	D<int> d_int;
	d_int.f();

	D<bool> d_bool;
	d_bool.f();

	D<double> d_double;
	d_double.f();
}
