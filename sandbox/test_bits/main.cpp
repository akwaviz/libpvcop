//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>

#include <pvcop/core/memarray.h>

/*****************************************************************************
 * main
 *****************************************************************************/

using bit_memarray = pvcop::core::memarray<bool>;

int main()
{
	std::cout << std::boolalpha;

	bit_memarray a(13);

	std::cout << "all 1" << std::endl;
	// a.set();

	for (const auto& b : a) {
		const bool v = *b;
		printf("%d\n", v);
	}

	std::cout << "all 0" << std::endl;
	// a.reset();

	for (const auto& b : a) {
		const bool v = *b;
		printf("%d\n", v);
	}

	std::cout << "just bit number 2 set" << std::endl;

	a[2] = true;

	bool b = a[2];
	std::cout << "a[2]: " << b << std::endl;

	for (const auto& b : a) {
		const bool v = b;
		printf("%d\n", v);
	}

	std::cout << "for à l'ancienne" << std::endl;
	for (const bit_memarray::iterator& b = a.begin(); b != a.end(); ++b) {
		const bool v = b;
		printf("%d\n", v);
	}
	return 0;
}
