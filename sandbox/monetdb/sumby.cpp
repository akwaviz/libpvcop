//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <tbb/blocked_range.h>
#include <tbb/parallel_reduce.h>
#include <tbb/parallel_for.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <monetdb/monetdb_config.h>
#include <monetdb/gdk.h>

#ifdef __cplusplus
}
#endif

#include <iostream>

#define BSIZE (20)
constexpr size_t nthreads = 3;

double now()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	return (double)tv.tv_sec + (tv.tv_usec / 1000000.);
}

BAT* load_bat_from_csv_col(int type, const char* path)
{
	size_t buffer_size = 2 * 1024 * 1024;
	char* buffer = (char*)malloc(buffer_size * sizeof(char));

	// Create a brand new BAT
	BAT* b = BATnew(TYPE_void, type, BSIZE);
	if (b == 0) {
		printf("BATnew failed\n");
		return NULL;
	}
	BATsetaccess(b, BAT_WRITE);

	// Open CSV file
	FILE* fp = fopen(path, "r");
	if (fp == NULL) {
		perror("fopen");
		return NULL;
	}

	// Copy values into the BAT
	unsigned int i = 0;
	while (i < BSIZE && feof(fp) == 0) {

		getline(&buffer, &buffer_size, fp);
		buffer[strlen(buffer) - 1] = '\0';

		if (type == TYPE_lng) {
			long v = atol(buffer); // ATOMfromstr(TYPE_lng, buffer, strlen(buffer), &v);
			BUNappend(b, (void*)&v, true);
		} else {
			BUNappend(b, (void*)buffer, true);
		}

		// tfastins_nocheck(b, r + i, buffer, Tsize(b)); // (1) /!\ Don't forget to do a
		// 'BATsetcount'
		// afterwards or use
		// 'BUNappend' instead of 'tfastins_nocheck' (slower)

		i++;
	}

	BATseqbase(b, 0);

	// Save BAT
	b = BATsave(b);
	if (b == NULL) {
		printf("BATsave fails\n");
		return NULL;
	}

	// Set READ access
	b = BATsetaccess(b, BAT_READ);
	if (b == NULL) {
		printf("BATsetaccess failed\n");
		return NULL;
	}

	free(buffer);

	return b;
}

BAT* generate_selection()
{
	BAT* dummy = NULL;
	BAT* b = tbb::parallel_deterministic_reduce(
	    tbb::blocked_range<size_t>(tbb::blocked_range<size_t>(0, nthreads, 1)), dummy,
	    [&](const tbb::blocked_range<size_t>& th, BAT*) -> BAT* {

		    size_t th_num = th.begin();
		    size_t range = BSIZE / nthreads;
		    size_t start = th_num * range;
		    size_t stop = th_num == (nthreads - 1) ? BSIZE : start + range;

		    BAT* b = BATnew(TYPE_void, TYPE_bit, (stop - start));
		    if (b == 0) {
			    printf("BATnew failed\n");
			    return NULL;
		    }

		    BATsetaccess(b, BAT_WRITE);

		    for (size_t row = start; row < stop; row++) {
			    bool s = /*row % 100 == 0*/ true;
			    BUNappend(b, (void*)&s, true);
		    }
		    BATseqbase(b, start);

		    return b;
		},
	    [](BAT* b1, BAT* b2) -> BAT* {
		    BATappend(b1, b2, false);
		    return b1;
		});

	return b;
}

BAT* parallel_sumby(BAT* b1, BAT* b2)
{
	using partial_grouping_result_t = std::pair<BAT*, BAT*>;

	partial_grouping_result_t partial_results[nthreads];

	BATprint(b1);
	BATprint(b2);

#define PARALLEL 0

#if PARALLEL
    tbb::parallel_for(tbb::blocked_range<size_t>(tbb::blocked_range<size_t>(0, nthreads, 1)),
		[&](tbb::blocked_range<size_t> const& th) {
		size_t th_num = th.begin();
#else
	for (size_t th_num = 0; th_num < nthreads; th_num++) {
		size_t range = BSIZE / nthreads;
		size_t start = th_num * range;
		size_t stop = th_num == (nthreads - 1) ? BSIZE : start + range;

		BAT* b1slice = BATslice(b1, start, stop);
		BAT* b2slice = BATslice(b2, start, stop);

		// BATGroup
		BAT *groups, *extents, *histo;
		BATgroup(&groups, &extents, &histo, b1slice, NULL, NULL, NULL);

		std::cout << "@groups:" << std::endl;
		BATprint(groups);
		std::cout << "@extents:" << std::endl;
		BATprint(extents);
		std::cout << "@histo:" << std::endl;
		BATprint(histo);

		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;

		BAT* group_sum = BATgroupsum(b2slice, groups, extents, NULL, TYPE_lng, false, false);

		// BATleftfetchjoin
		BAT* groups_leftfetchjoin = BATleftfetchjoin(extents, b1slice, 0);

		// Store partial results:
		partial_results[th_num].first = std::move(groups_leftfetchjoin);
		partial_results[th_num].second = std::move(group_sum);
	}
#if PARALLEL
, tbb::simple_partitioner());
#endif
#endif

		double start = now();

		BAT* final_groups_leftfetchjoin = partial_results[0].first;
		BATsetaccess(final_groups_leftfetchjoin, BAT_WRITE);

		BAT* final_groupcount = partial_results[0].second;
		BATsetaccess(final_groupcount, BAT_WRITE);

		for (size_t i = 1; i < nthreads; i++) {
			BAT* partial_groups_leftfetchjoin = partial_results[i].first;
			BATappend(final_groups_leftfetchjoin, partial_groups_leftfetchjoin, false);

			BAT* partial_groupsum = partial_results[i].second;
			BATappend(final_groupcount, partial_groupsum, false);
		}

		// Final steps (BATgroup + BATleftfetchjoin + BATgroupsum)
		BAT *groups, *extents, *histo;
		BATgroup(&groups, &extents, &histo, final_groups_leftfetchjoin, NULL, NULL, NULL);

		BAT* b1_final = BATleftfetchjoin(extents, final_groups_leftfetchjoin, 0);

		BAT* b2_final =
		    BATgroupsum(final_groupcount, groups, extents, NULL, TYPE_lng, false, false);
		printf("BATcount(extents)=%ld\n", BATcount(extents));

		double end = now();
		printf("MERGE took %f seconds\n", (end - start));

		{
			BATiter b1_iter = bat_iterator(b1_final);
			BATiter b2_iter = bat_iterator(b2_final);
			BATiter b1_extents_iter = bat_iterator(extents);
			for (BUN i = (BUN)0; i < (BUN)BATcount(extents); i++) {
				const oid* h = (oid*)BUNhead(b1_extents_iter, i);
				const oid* t = (oid*)BUNtail(b1_extents_iter, i);

				const char* ip = (char*)BUNtail(b1_iter, *h);
				const wrd* count = (wrd*)BUNtail(b2_iter, i);
				printf("%lu %lu %s (%ld)\n", *h, *t, ip, *count);

				BAT* bsubsel = BATsubselect(b1, NULL, ip, ip, 1, 1, 0);
				BAT* bproj = BATproject(bsubsel, b2);
				BATiter bproj_iter = bat_iterator(bproj);
				for (BUN j = (BUN)0; j < (BUN)BATcount(bproj); j++) {
					const wrd* totalbyte = (wrd*)BUNtail(bproj_iter, j);
					printf("\t%lu\n", *totalbyte);
				}
			}
		}

		return b1;
}

int main()
{
		opt* o = nullptr;
		int l = 0;
		double start, end;

		char cwd[1024];
		if (getcwd(cwd, sizeof(cwd)) != NULL)

			// Setup options
			l = mo_add_option(&o, l, opt_cmdline, "gdk_dbpath", "/tmp/monetdb");
		l = mo_add_option(&o, l, opt_cmdline, "gdk_single_user", "yes");

		if (!GDKinit(o, l)) {
			return 1;
		}

		// Load BATs from CSV
		start = now();
		char path1[1024];
		strcpy(path1, cwd);
		strcat(path1, "/srcip_20.csv");
		BAT* bat_str = load_bat_from_csv_col(TYPE_str, path1);
		end = now();
		printf("insertion (%lu values) took %f seconds\n", BATcount(bat_str), (end - start));

		start = now();
		char path2[1024];
		strcpy(path2, cwd);
		strcat(path2, "/totalbytes_20.csv");
		BAT* bat_lng = load_bat_from_csv_col(TYPE_lng, path2);
		end = now();
		printf("insertion (%lu values) took %f seconds\n", BATcount(bat_lng), (end - start));

		start = now();
		parallel_sumby(bat_str, bat_lng);
		end = now();
		printf("sumby took %f seconds\n", (end - start));

		return 0;
}
