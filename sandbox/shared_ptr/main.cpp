//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <memory>
#include <iostream>

class test
{
  public:
	using p = std::shared_ptr<test>;
	using pp = std::shared_ptr<std::shared_ptr<test>>;

  public:
	test(size_t n) : _n(n) {}
	~test() { std::cout << "[deleting test(" << _n << ")]" << std::endl; }

  public:
	void print(const std::string& s) { std::cout << s << "=test(" << _n << ")" << std::endl; }

  private:
	size_t _n;
};

int main()
{
	{
		test::p ptr1(new test(42));
		test::p ptr2 = ptr1;

		ptr1->print("ptr1");
		ptr2->print("ptr2");

		std::cout << "changing pointed data for a specific instance of test::p" << std::endl;
		ptr1.reset(new test(43));

		ptr1->print("ptr1");
		ptr2->print("ptr2");
	}

	std::cout << "---" << std::endl;

	{
		test::pp ptr1(new test::p(new test(42)));
		test::pp ptr2 = ptr1;

		ptr1.get()->get()->print("ptr1");
		ptr2.get()->get()->print("ptr2");

		std::cout << "changing pointed data for all instances of test::pp" << std::endl;
		ptr1.get()->reset(new test(43));

		ptr1.get()->get()->print("ptr1");
		ptr2.get()->get()->print("ptr2");
	}
}
