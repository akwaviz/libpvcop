//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>
#include <vector>

#include <sys/time.h>

static constexpr size_t COUNT = 1000000000;

static inline double now()
{
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return (double)tv.tv_sec + (tv.tv_usec / 1000000.);
}

int main()
{
	double t1, t2;

	std::vector<uint64_t> v_uint64;
	v_uint64.reserve(COUNT);

	std::vector<uint64_t> v2_uint64;
	v2_uint64.reserve(COUNT);

	std::vector<double> v_double;
	v_double.reserve(COUNT);

	srand(time(NULL));

	for (size_t i = 0; i < COUNT; i++) {
		v_uint64[i] = static_cast<uint64_t>(rand());
	}

	t1 = now();
	for (size_t i = 0; i < COUNT; i++) {
		v2_uint64[i] = v_uint64[i];
	}
	t2 = now();
	std::cout << "reference time : " << (t2 - t1) << " sec." << std::endl;

	t1 = now();
	for (size_t i = 0; i < COUNT; i++) {
		v_double[i] = (double)v_uint64[i];
	}
	t2 = now();
	std::cout << "casting uint64_t to double done in " << (t2 - t1) << " sec." << std::endl;
}
