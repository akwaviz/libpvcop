//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <iostream>

#include <pvcop/core/memarray.h>

#include <type_traits>
#include <functional>

/*****************************************************************************
 *
 *****************************************************************************/

/* not in array<T> because memarray could not exist.
 */
template <typename F, typename T, typename... P>
pvcop::core::memarray<typename std::result_of<F(T, P...)>::type>
map(pvcop::core::array<T>& b, F&& f, P&&... p)
{
	using ret_type = pvcop::core::memarray<typename std::result_of<F(T, P...)>::type>;

	if (!b) {
		return ret_type();
	}

	ret_type res = ret_type(b.size());

	for (size_t i = 0; i < b.size(); ++i) {
		res[i] = f(b[i], p...);
	}

	return res;
}

/* like ::map() but the value index is known
 */
template <typename F, typename T, typename... P>
pvcop::core::memarray<typename std::result_of<F(T, size_t, P...)>::type>
visit(pvcop::core::array<T>& b, F&& f, P&&... p)
{
	using ret_type = pvcop::core::memarray<typename std::result_of<F(T, size_t, P...)>::type>;

	if (!b) {
		return ret_type();
	}

	ret_type res = ret_type(b.size());

	for (size_t i = 0; i < b.size(); ++i) {
		res[i] = f(b[i], i, p...);
	}

	return res;
}

/*****************************************************************************
 * main
 *****************************************************************************/

class functor
{
  public:
	uint16_t operator()(uint32_t a, uint8_t s) { return (uint16_t)(a >> s); }
};

using farray = pvcop::core::array<float>;
using fmemarray = pvcop::core::memarray<float>;

bool do_things(farray& in, fmemarray& out)
{
	out = map(in, [](float v) -> float { return v + 1.0; });
	return out;
}

int main()
{
	std::cout << std::boolalpha;

	// must not compile:
	// pvcop::core::array<uint32_t> aa;

	pvcop::core::memarray<uint32_t> ma(1);

	ma[0] = 65538;

	auto resl = map(ma, [](uint32_t a, uint8_t s) -> uint16_t { return (uint16_t)(a >> s); }, 16);

	std::cout << "lambda result: " << resl[0] << std::endl;

	std::function<uint16_t(uint32_t, uint8_t)> b =
	    [](const uint32_t a, const uint8_t s) -> uint16_t { return (uint16_t)(a >> s); };

	auto resb = map(ma, b, 16);

	std::cout << "std::function result: " << resb[0] << std::endl;

	functor f;

	auto resf = map(ma, f, 16);

	std::cout << "functor result: " << resf[0] << std::endl;

	// must not compile:
	// pvcop::core::memarray<uint32_t> mb(ma);

	// must not compile:
	// pvcop::core::memarray<uint32_t> mc = ma;

	pvcop::core::array<uint32_t> a = ma;

	std::cout << "a.size() = " << a.size() << std::endl;

	fmemarray fma(256);
	std::cout << "fma <=> " << fma << std::endl;

	fmemarray fmb;
	std::cout << "fmb <=> " << fmb << std::endl;

	std::cout << "do_things(fma, fmb) returns " << do_things(fma, fmb) << std::endl;
	std::cout << "fmb <=> " << fmb << std::endl;

	// must not compile:
	// fmemarray* fmaa = new fmemarray [256];

	return 0;
}
