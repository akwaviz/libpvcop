//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <pvcop/pvcop.h>

#include <pvlogger.h>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <chrono>
#include <thread>

#include <stdlib.h>
#include <time.h>

#include <tbb/parallel_for.h>
#include <tbb/task_scheduler_init.h>
#include <pvhwloc.h>

using namespace pvcop;

static inline double now()
{
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return (double)tv.tv_sec + (tv.tv_usec / 1000000.);
}

db::array random_time(db::array& array)
{
	auto& native_array = array.to_core_array<uint32_t>();

	for (size_t i = 0; i < native_array.size(); i++) {
		native_array[i] = static_cast<uint32_t>(rand());
	}

	return std::move(array);
}

void print_time(db::array& array)
{
	auto& native_array = array.to_core_array<uint32_t>();
	char time_str[100];

	for (size_t i = 0; i < native_array.size(); i++) {
		time_t t = static_cast<time_t>(native_array[i]);
		if (std::strftime(time_str, sizeof(time_str), "%A %c", std::localtime(&t))) {
			pvlogger::info() << time_str << std::endl;
		}
	}
}

#define LOOP_SEQUENTIAL 1
#define LOOP_OMP 0
#define LOOP_TBB 0

#define USE_STRFTIME 0

#define JUST_SLEEP 0

using mapping_type = uint8_t;

core::memarray<mapping_type> compute_mapping(db::array& array)
{
	auto& native_array = array.to_core_array<uint32_t>();
	core::memarray<mapping_type> mapping_array(array.size());

	double t1, t2;
	t1 = now();

#if LOOP_SEQUENTIAL
	pvlogger::info() << "(sequential loop)" << std::endl;
	pvlogger::info() << "range=" << 0 << ".." << native_array.size() << std::endl;
	tm local_tm;
	for (size_t i = 0; i < native_array.size(); i++) {

#else
	const size_t nthreads = pvhwloc::core_count();
	pvlogger::info() << "nthreads=" << nthreads << std::endl;
	std::size_t grain_size = std::max(nthreads, (native_array.size() / nthreads) & ~15);
	pvlogger::info() << "native_array.size()=" << native_array.size() << std::endl;
	pvlogger::info() << "grain_size=" << grain_size << std::endl << std::endl;
#endif

#if LOOP_OMP
		pvlogger::info() << "(OMP loop)" << std::endl;
#pragma omp parallel for num_threads(nthreads) schedule(static, grain_size)
		for (size_t i = 0; i < native_array.size(); i++) {
			tm local_tm;
#elif LOOP_TBB
		pvlogger::info() << "(TBB loop)" << std::endl;
	// tbb::task_scheduler_init tsi(nthreads);
	tbb::parallel_for(tbb::blocked_range<size_t>(0, native_array.size(), grain_size),
			[&](tbb::blocked_range<size_t> const& range) {
			pvlogger::info() << "range=" << range.begin() << ".." << range.end() << std::endl;
			tm local_tm;
			for (size_t i = range.begin(); i != range.end(); i++) {
#endif

#if JUST_SLEEP
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
#else
				const time_t t = static_cast<const time_t>(native_array[i]);
#if USE_STRFTIME
				char time_str[100];
				if (std::strftime(time_str, sizeof(time_str), "%w", gmtime_r(&t, &local_tm))) {
					mapping_array[i] = std::atol(time_str);
				}
#else
				gmtime_r(&t, &local_tm);
				mapping_array[i] = local_tm.tm_wday;
#endif // JUST_SLEEP
#endif // USE_STRFTIME
		}
#if LOOP_TBB == 1
	}
	    , tbb::simple_partitioner());
#endif

	    t2 = now();

	    std::cout << "compute mapping time: " << (t2 - t1) << std::endl;

	    return std::move(mapping_array);
}

template <typename T>
void print_mapping(const core::array<T>& array)
{
	for (size_t i = 0; i < array.size(); i++) {
		pvlogger::info() << (uint32_t)array[i] << std::endl;
	}
}

int main()
{
	setenv("TZ", "GMT", 1); // set TZ
	tzset();

	std::locale::global(std::locale("C"));

	srand(time(NULL));

	db::array time_array =
	    db::array(pvcop::db::type_traits::type<uint32_t>::get_type_id(), 100000000);

	time_array = random_time(time_array);

	core::memarray<mapping_type> mapping = compute_mapping(time_array);

	// print_time(time_array);
	// print_mapping(mapping);
}
