/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PVCOP_UTILS_FORMATTER_H
#define PVCOP_UTILS_FORMATTER_H

#include "formatter_base.h"

namespace pvcop
{

namespace utils
{

/**
 * This class defines a way to convert binary values to and from a
 * null-terminated string.
 */
template <typename T>
class formatter : public formatter_base
{
  public:
	/**
	 * Default constructor
	 *
	 * @param name the formatter name
	 */
	formatter(const char* name) : formatter_base(name) {}

	/**
	 * Default destructor
	 */
	~formatter() {}

  public:
	/**
	 * override pvcop::utils::formatter_base::from_string
	 *
	 * @param str a null-terminated string
	 * @param base_addr a memory address
	 * @param i an index related to the "undereferenced" memory address
	 *
	 * @return true on success; false otherwise
	 */
	bool from_string(const char* str, void* base_addr, size_t i) const override final
	{
		T* data = static_cast<T*>(base_addr);
		return convert_from_string(str, data[i]);
	}

	/**
	 * override pvcop::utils::formatter_base::to_string
	 *
	 * @param str a null-terminated string
	 * @param base_addr a memory address
	 * @param i an index related to the "undereferenced" memory address
	 *
	 * @return true on success; false otherwise
	 */
	bool to_string(char* str, const void* base_addr, size_t i) const override final
	{
		const T* data = static_cast<const T*>(base_addr);
		return convert_to_string(str, data[i]);
	}

  protected:
	/**
	 * Extract a value from a null-terminated string.
	 *
	 * @param str a null-terminated string
	 * @param value the value to format
	 *
	 * @return true on success; false otherwise
	 */
	virtual bool convert_from_string(const char* str, T& value) const = 0;

	/**
	 * Format a value to a null-terminated string.
	 *
	 * @param str a null-terminated string
	 * @param value the value to format
	 *
	 * @return true on success; false otherwise
	 */
	virtual bool convert_to_string(char* str, const T& value) const = 0;
};
}
}

#endif // PVCOP_UTILS_FORMATTER_H
