/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PVCOP_UTILS_FORMATTER_NUMBER_H
#define PVCOP_UTILS_FORMATTER_NUMBER_H

#include "formatter.h"

#include <type_traits>

#include <cstdlib>
#include <cstdio>

namespace pvcop
{

namespace utils
{

/**
 * @todo ato* should be rewritten from scratch to accept a string length parameter
 * to permit to extract "124" from "124876" because parameters are ("124876", 3)
 */

/**
 */
/**
 * This class defines a formatter for scalar values.
 */
template <typename T>
class formatter_number : public formatter<T>
{
  public:
	/**
	 * Default constructor
	 *
	 * @param name the formatter name
	 */
	formatter_number(const char* name) : formatter<T>(name) {}

	/**
	 * Default destructor
	 */
	~formatter_number() {}

  protected:
	/**
	 * Overrides pvcop::utils::formatter::from_string
	 *
	 * @param str a null-terminated string
	 * @param value the value to format
	 *
	 * @return true on success; false otherwise
	 */
	bool convert_from_string(const char* str, T& value) const override
	{
		value = to_number<T>(str);
		return true;
	}

	/**
	 * Overrides pvcop::utils::formatter::to_string
	 *
	 * @param str a null-terminated string
	 * @param value the value to format
	 *
	 * @return true on success; false otherwise
	 */
	bool convert_to_string(char* str, const T& value) const override
	{
		return from_number<T>(str, value);
	}

  private:
	template <class U>
	typename std::enable_if<std::is_floating_point<U>::value, U>::type
	to_number(const char* str) const
	{
		return atof(str);
	}

	template <class U>
	typename std::enable_if<std::is_integral<U>::value, U>::type to_number(const char* str) const
	{
		return atol(str);
	}

	template <class U>
	typename std::enable_if<std::is_floating_point<U>::value, U>::type from_number(const char* str,
	                                                                               U value) const
	{
		return sprintf(str, "%g", value) >= 0;
	}

	template <class U>
	typename std::enable_if<std::is_integral<U>::value, U>::type from_number(char* str,
	                                                                         U value) const
	{
		return sprintf(str, "%d", value) >= 0;
	}
};
}
}

#endif // PVCOP_UTILS_FORMATTER_NUMBER_H
