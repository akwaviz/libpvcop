/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PVCOP_UTILS_FORMATTER_BASE_H
#define PVCOP_UTILS_FORMATTER_BASE_H

#include <cstddef>

namespace pvcop
{

namespace utils
{

/**
 * This class defines the interface to extract and format values from and to
 * a null-terminated string.
 */
class formatter_base
{
  public:
	/**
	 * Default constructor
	 *
	 * @param name the formatter name
	 */
	formatter_base(const char* name) : _name(name) {}

	/**
	 * Default destructor
	 */
	virtual ~formatter_base() {}

  public:
	const char* name() const { return _name; }

  protected:
	/**
	 * Extract a value from a null-terminated string and store it at a
	 * dereferenced memory address.
	 *
	 * @param str a null-terminated string
	 * @param base_addr a memory address
	 * @param i an index related to the "undereferenced" memory address
	 *
	 * @return true on success; false otherwise
	 */
	virtual bool from_string(const char* str, void* base_addr, size_t i) const = 0;

	/**
	 * Format a value to a null-terminated string by reading it from a
	 * dereferenced memory address.
	 *
	 * @param str a null-terminated string
	 * @param base_addr a memory address
	 * @param i an index related to the "undereferenced" memory address
	 *
	 * @return true on success; false otherwise
	 */
	virtual bool to_string(char* str, const void* base_addr, size_t i) const = 0;

  private:
	const char* _name;
};
}
}

#endif // PVCOP_UTILS_FORMATTER_BASE_H
