/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __PVCOP_DB_128BITS_SUPPORT_H
#define __PVCOP_DB_128BITS_SUPPORT_H

#include <pvcop/core/impl/cast.h>

namespace pvcop
{

namespace db
{

using uint128_t = __uint128_t;

} // namespace db

} // namespace pvcop

namespace std
{

template <>
struct hash<pvcop::db::uint128_t> {
	std::size_t operator()(const pvcop::db::uint128_t& v) const
	{
		using bitset_t = pvcop::core::bitset_cast_t<pvcop::db::uint128_t>;
		return std::hash<bitset_t::value_type>()(bitset_t(v).cast());
	}
};

template <>
struct is_arithmetic<pvcop::db::uint128_t> : public is_arithmetic<uint64_t> {
};

template <>
struct is_integral<pvcop::db::uint128_t> : public is_integral<uint64_t> {
};

template <>
struct numeric_limits<pvcop::db::uint128_t> {
	static pvcop::db::uint128_t max() { return (pvcop::db::uint128_t)-1; }
	static pvcop::db::uint128_t min() { return 0; }
	static pvcop::db::uint128_t lowest() { return min(); }
	static constexpr const bool is_integer = true;
	static constexpr const int digits = 128;
	static constexpr bool is_specialized = true;
};

} // namespace std

#endif
