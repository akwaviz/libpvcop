cmake_minimum_required(VERSION 2.8)

project(libpvcop)

if (WIN32)
	message(ERROR, "WIN32 plateforms are not supported")
endif(WIN32)

include(CTest)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)

###############################################################################
# Define the Intel TBB Library
###############################################################################
include(CMakeOptions.txt)
include(CMakeCompilers.txt)
include(cmake/Program.cmake)
include(CMakePackage.txt)

find_package(Doxygen)
find_package(OpenMP)
find_package(TBB 4.4 REQUIRED)
find_package(ICU 57.1 COMPONENTS data i18n REQUIRED)
find_package(Tcmalloc REQUIRED)
set(Boost_USE_STATIC_LIBS       OFF)
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
add_definitions(-DBOOST_PROGRAM_OPTIONS_DYN_LINK)
if (${CMAKE_INSTALL_PREFIX} STREQUAL "/app")
	SET(BOOST_INCLUDEDIR /app/include)
endif()
find_package(Boost 1.53 COMPONENTS date_time)

SET(USE_LIBISTRCONV 1)
if(USE_LIBISTRCONV)
add_definitions(-DLIB_ISTRCONV)
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/external/libistrconv")
SET(istrconv_LIBRARY
    "${CMAKE_CURRENT_SOURCE_DIR}/external/libistrconv/libistrconv.a;"
    "${CMAKE_CURRENT_SOURCE_DIR}/external/libistrconv/libirc.a;"
)
endif(USE_LIBISTRCONV)

###############################################################################
# Python3 + Numpy
###############################################################################
find_package(Python3 REQUIRED COMPONENTS Development)
include_directories(SYSTEM AFTER ${Python3_INCLUDE_DIRS})
find_package (Python3 COMPONENTS Interpreter NumPy)
include_directories(SYSTEM AFTER ${Python_NumPy_INCLUDE_DIRS})

###############################################################################
# inendi-utils
###############################################################################
if (NOT DEFINED INENDI_UTILS)
	message(STATUS "Configuring inendi-utils...")
	SET(PVLOGGER_LIBRARIES pvlogger)
	SET(PVHWLOC_LIBRARIES pvhwloc)
	include_directories(${CMAKE_SOURCE_DIR}/inendi-utils/libpvlogger/include)
	include_directories(${CMAKE_SOURCE_DIR}/inendi-utils/libpvhwloc/include)
	add_subdirectory(inendi-utils)
endif()

message(STATUS "Build type is ${CMAKE_BUILD_TYPE}")
message(STATUS "Architecture flag: ${CMAKE_CXX_ARCH_FLAGS}")
message(STATUS "use 64 bits indexes: ${INDEXES_64BITS}")

if(DOXYGEN_FOUND)
	message(STATUS "Doxygen found")
	add_subdirectory(doc)
endif(DOXYGEN_FOUND)

include_directories(AFTER include)
include_directories(AFTER third_party)
include_directories(AFTER ${TBB_INCLUDE_DIRS})
include_directories(AFTER ${ICU_INCLUDE})

message(STATUS "ICU found: " ${ICU_FOUND})
if(ICU_FOUND)
  message(STATUS "ICU include: ${ICU_INCLUDE}")
  message(STATUS "ICU libraries: ${ICU_LIBRARIES}")
endif(ICU_FOUND)

if (OPENMP_FOUND)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  set(OpenMP_LIBRARIES "-fopenmp=libiomp5")
  add_definitions("-fopenmp=libiomp5")
else()
  find_package(OpenMP REQUIRED)
  set(OpenMP_LIBRARIES ${OpenMP_CXX_FLAGS})
  add_definitions(${OpenMP_CXX_FLAGS})
endif()
endif()

add_subdirectory(src)
add_subdirectory(include)

add_subdirectory(tests)
add_subdirectory(sandbox)

INSTALL(FILES pvcop-config.cmake DESTINATION lib/pvcop/cmake/ COMPONENT dev)
