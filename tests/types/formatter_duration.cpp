//
// MIT License
//
// © ESI Group, 2015
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <pvcop/pvcop.h>
#include <pvcop/types/duration.h>

#include "formatter_check.h"

static boost::posix_time::time_duration generate_valid_durations(uint64_t random_value)
{
	uint32_t hour = (random_value % 24);
	uint8_t minute = (random_value >> 24 & 0xFF) % 60;
	uint8_t sec = (random_value >> 16 & 0xFF) % 60;
	uint16_t msec = (random_value & 0xFFFF) % 1000;

	return boost::posix_time::time_duration(hour, minute, sec, msec);
}

int main()
{
	// Bench formatter_duration
	std::cout << "formatter_duration:" << std::endl;
	pvcop::types::formatter_duration dur("");
	bench_formatter<boost::posix_time::time_duration, uint64_t>(
	    dur, std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max(),
	    generate_valid_durations);
}
